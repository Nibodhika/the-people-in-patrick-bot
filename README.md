# What is this?

This is a discord bot to play The People in Patrick.

The People in Patrick is a one-page RPG developed by the people at [i3RPG](http://i3rpg.libsyn.com/), you can get the rules [here](https://sillygoosecontent.com/wp-content/uploads/2019/08/The-People-in-Patrick-i3RPG.pdf) and check out their podcast for other similar games, quoting from their post at [reddit](https://www.reddit.com/r/rpg/comments/d9yf84/the_people_in_patrick_rules_pdf_tiny_aliens/)

>>>
100 Word Pitch:

You play as a crew of 2 inch tall aliens piloting a jaeger of uncannily human appearance called Patrick. Your mission is to blend in with the human population and learn more about their habits. Unfortunately, you are not all drift compatible and struggle to operate Patrick as intended, leading to horribly awkward human situations you need to gracefully wriggle out of.

If you're after your next chilled comedy RPG experience then check out the rules to The People in Patrick! It's lightly GM driven with Madlibs-inspired mechanics and only requires pen and paper to play. Think Madlibs meets Pacific Rim meets Octodad.
>>>

# How to use it?

After the bot is in your channel give the `start` command, the bot will add everyone in that channel to the game (do notice that everyone needs to answer one option before the answer gets drawn).

The person who called the start becomes the gm, and he can now use the `option` command. If called without parameters it will prompt the users for an "Action" via DM. For more complex stuff, like prompting an upper and lower body action, or several options for conversation, you can give the options sepparated by space using quotes to wrap multi word options, e.g.

```
!option "Upper Body" "Lower Body"
```

This will prompt players for an "Upper Body" option, and afterwards with a "Lower Body" option. After everyone has selected their options the bot will randomly pick the options using the rules described in the PDF and show them in the channel for everyone to see (Note that this means that it can pick the Upper Body action of one player and the Lower Body action of another, which is a recipe for fun).


# Where is the bot?

Eventually I will probably make a bot available that can be simply invited to your channel, in the meantime if you want to test it out you'll need `python` and `pip` installed.

## (Optional) Virtualenv setup

Create and activate the virtualenv

```bash
virtualenv env -p python3
source env/bin/activate
```

## Create config.py

config.py is a file that has the bot configurations, it should be by the side of main.py at the root of the project, and it's content should be similar to:

```python
PREFIX = '!'
TOKEN = 'Your bot token goes here'
```

Where `PREFIX` is the prefix you want for the bot commands and `TOKEN` is the token for your discord bot (If you don't know how to create a discord bot you can check it out [here](https://discordpy.readthedocs.io/en/latest/discord.html#discord-intro) ) 

## Run the bot

```bash
python ./main.py
```
