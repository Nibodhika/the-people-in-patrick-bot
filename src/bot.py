import discord
from discord.ext import commands

from config import *
from . import database

database.init('database.db')

bot = commands.Bot(command_prefix='!')

OPTIONS_TEMPLATE = 'Give me an option for "{}"'


async def check_gm(ctx):
    """
    Checks if there is a game running and if it was the gm that sent the message
    :param ctx:
    :return:
    """
    gm_id = database.get_gm(ctx.channel.id)
    if gm_id is None:
        await ctx.send("No game currently running, use the start command to start one")
    elif ctx.author.id != gm_id:
        await ctx.send("Only the GM can use this command")
    else:
        return True
    return False


def do_draw(options, game):
    embed = discord.Embed(title="Result")
    for option_id, option_name in options:
        answers = database.draw(game, option_id)
        if answers is False:
            value = "Not enough people have answered to perform a draw for this option"
        elif answers is None:
            value = "Option did not matched the current channel, if this is not a redraw please report it"
        else:
            user_answers = []
            for a in answers:
                user_id, user_answer = a
                user = bot.get_user(user_id)
                user_answers.append("{}: {}".format(user.mention, user_answer))
            value = '\n'.join(user_answers)
        name = "{} ({})".format(option_name, option_id)
        embed.add_field(name=name, value=value, inline=False)
    return embed


@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')


@bot.event
async def on_message(message):
    if message.author == bot.user:
        return

    if isinstance(message.channel, discord.DMChannel):
        game, next_option, waiting = database.add_answer(message.author.id, message.content)
        if next_option:
            await message.channel.send(OPTIONS_TEMPLATE.format(next_option))
        elif waiting == 0:
            # This was the last answer from the last option, we can perform all drawings now
            options = database.get_unanswered_options(game)
            channel = bot.get_channel(game)
            embed = do_draw(options, game)
            await channel.send(embed=embed)
        elif game:
            await message.channel.send("Waiting other players")
        else:
            await message.channel.send("I'm not waiting on an answer from you")
    else:
        await bot.process_commands(message)


@bot.command()
async def start(ctx):
    """
    Starts a new game
    """
    members = [m.id for m in ctx.channel.members if not m.bot and m.id != ctx.author.id]
    database.start_game(ctx.channel.id, ctx.author.id, members)
    msg = "Starting new game"
    await ctx.send(msg)
    msg += ' from channel {}'.format(ctx.channel.mention)
    # Send via DM to each player
    players = database.get_players(ctx.channel.id)
    for p in players:
        user = bot.get_user(p)
        await user.send(msg)


@bot.command()
async def option(ctx, *args):
    """
    Gives an option to the players.
    """
    if not await check_gm(ctx):
        return

    options = args or ('Action',)
    players = database.get_players(ctx.channel.id)
    for o in options:
        database.create_option(ctx.channel.id, o, players)

    for p in players:
        user = bot.get_user(p)
        await user.send(OPTIONS_TEMPLATE.format(options[0]))


@bot.command()
async def who(ctx):
    """
    Gives a list of who hasn't answered the last option
    """
    waiting = database.get_waiting_players(ctx.channel.id)
    if len(waiting) == 0:
        await ctx.send("Not waiting for anyone")
    else:
        mentions = [bot.get_user(w).mention for w in waiting]
        msg = "Waiting for: {}".format(', '.join(mentions))
        await ctx.send(msg)


@bot.command()
async def draw(ctx):
    """
    Forces a draw to happen
    """
    game = ctx.channel.id
    options = database.get_unanswered_options(game)
    embed = do_draw(options, game)
    await ctx.send(embed=embed)


@bot.command()
async def redraw(ctx, *args):
    """
    Redraws one or many already drawn options
    """
    if len(args) == 0:
        ctx.send("You must specify at least one option id")
        return
    game = ctx.channel.id
    try:
        options = [int(i) for i in args]
    except ValueError:
        ctx.send("Option ids must be integers")
        return
    options_with_name = database.get_option_names(game, options)
    embed = do_draw(options_with_name, game)
    await ctx.send(embed=embed)


def run():
    bot.run(TOKEN)
