import json
import random


def test_database(database, game, mocker):
    """
    Tests the basic flow of a game by giving options and simulating answers and then simulating multiple draws to
    check the state of the bowl
    """
    p1, p2, p3 = game.players
    expected_initial_bowl = [p1] * 4 + [p2] * 4 + [p3] * 4 + [database.DOUBLE] * 3

    def check_bowl(expected_bowl, expected_used):
        database.c.execute('SELECT bowl,used FROM bowl WHERE game=?', (game.channel,))
        bowl_row = database.c.fetchone()
        assert bowl_row is not None
        bowl, used = [json.loads(b) for b in bowl_row]

        assert bowl == expected_bowl
        assert used == expected_used

    check_bowl(expected_initial_bowl, [])

    assert database.get_gm(game.channel) == game.gm
    assert database.get_players(game.channel) == game.players
    assert database.get_waiting_players(game.channel) == set()

    option = database.create_option(game.channel, 'Action', game.players)
    assert database.get_waiting_players(game.channel) == set(game.players)

    p1_answer = "Player1's answer"
    assert database.add_answer(p1, p1_answer) == (game.channel, None, 2)
    assert database.get_waiting_players(game.channel) == set([p2, p3])
    assert database.add_answer(p1, "Player1's second answer") == (None, None, None)

    p2_answer = "Player2's answer"
    assert database.add_answer(p2, p2_answer) == (game.channel, None, 1)
    assert database.get_waiting_players(game.channel) == set([p3])
    p3_answer = "Player3's answer"
    assert database.add_answer(p3, p3_answer) == (game.channel, None, 0)
    assert database.get_waiting_players(game.channel) == set()

    # Basic drew
    choice_mock = mocker.patch.object(random, 'choice', side_effect=[p1])
    drew = database.draw(game.channel, option)
    assert drew == [(p1, p1_answer)]
    expected_bowl = [p1] * 3 + [p2] * 4 + [p3] * 4 + [database.DOUBLE] * 3
    check_bowl(expected_bowl, [p1])

    # A drew with a double (note that we drew another dobuble but it's not cumulative
    choice_mock.side_effect = [database.DOUBLE, p3, database.DOUBLE, p3, p2]
    drew = database.draw(game.channel, option)

    # Note that this comes in p2,p3 order even if they were drew in p3, p2 order
    # This happens because they're pushed from the database in order
    assert drew == [(p2, p2_answer), (p3, p3_answer)]

    # Note that p1 was drawn in the previous, so it's still expected to be in the used
    expected_bowl = [p1] * 3 + [p2] * 3 + [p3] * 3 + [database.DOUBLE] * 2
    expected_used = [p1, database.DOUBLE, p3, p2]
    check_bowl(expected_bowl, expected_used)

    # Let's draw some other times to ensure the bowl gets reset if needed
    discard = [p1]*3 + [p2]*3
    expected_answer = {
        p1: p1_answer,
        p2: p2_answer,
    }
    choice_mock.side_effect = discard
    for d in discard:
        drew = database.draw(game.channel, option)
        assert drew == [(d, expected_answer[d])]

    expected_bowl = [p3] * 3 + [database.DOUBLE] * 2
    expected_used = [p1, database.DOUBLE, p3, p2] + discard
    check_bowl(expected_bowl, expected_used)

    # If we pull a p3 now we should be fine
    choice_mock.side_effect = [p3]
    drew = database.draw(game.channel, option)
    assert drew == [(p3, p3_answer)]

    expected_bowl = [p3] * 2 + [database.DOUBLE] * 2
    expected_used = [p1, database.DOUBLE, p3, p2] + discard + [p3]
    check_bowl(expected_bowl, expected_used)

    # However if we pull a double we need to reset to be able to pull two different options
    choice_mock.side_effect = [database.DOUBLE, p3, p3, database.DOUBLE, p1]
    drew = database.draw(game.channel, option)
    assert drew == [(p1, p1_answer), (p3, p3_answer)]

    # Note that the bowl began to become scrambled because the used pile was extended to the bowl
    expected_bowl = [105, 0, 0, 105, 32, 26, 26, 26, 32, 32, 32, 105, 0, 105]
    # Note that the double and p3 were drawn before the reset, so the used only contains p1 which was drawn after
    expected_used = [p1]
    check_bowl(expected_bowl, expected_used)


def test_multiple_options(database, game):
    """
    Tests that multiple options are working correctly by creating a multiple option and simulating player answering it
    also tests get_waiting_players.
    """
    p1, p2, p3 = game.players
    options_names = ("Upper Body", "Lower Body")
    options = []
    for o in options_names:
        options.append((database.create_option(game.channel, o, game.players), o))

    assert database.get_unanswered_options(game.channel) == options
    assert database.get_waiting_players(game.channel) == set(game.players)

    p1_u_answer = "Player1's Upper answer"
    p1_l_answer = "Player1's Lower answer"
    assert database.add_answer(p1, p1_u_answer) == (game.channel, "Lower Body", 2)
    assert database.get_waiting_players(game.channel) == set(game.players)
    assert database.add_answer(p1, p1_l_answer) == (game.channel, None, 2)
    assert database.get_waiting_players(game.channel) == set([p2, p3])
    assert database.add_answer(p1, "wrong") == (None, None, None)

    p2_u_answer = "Player2's Upper answer"
    assert database.add_answer(p2, p2_u_answer) == (game.channel, "Lower Body", 1)
    assert database.get_waiting_players(game.channel) == set([p2, p3])

    p3_u_answer = "Player3's Upper answer"
    assert database.add_answer(p3, p3_u_answer) == (game.channel, "Lower Body", 0)
    assert database.get_waiting_players(game.channel) == set([p2, p3])

    p2_l_answer = "Player2's Lower answer"
    assert database.add_answer(p2, p2_l_answer) == (game.channel, None, 1)
    assert database.get_waiting_players(game.channel) == set([p3])

    p3_l_answer = "Player3's Lower answer"
    assert database.add_answer(p3, p3_l_answer) == (game.channel, None, 0)
    assert database.get_waiting_players(game.channel) == set()

    # Just adding the answers does not actually answer them, we need to call draw for that
    assert database.get_unanswered_options(game.channel) == options

    database.draw(game.channel, options[0][0])

    assert database.get_unanswered_options(game.channel) == [options[1]]

    database.draw(game.channel, options[1][0])
    assert database.get_unanswered_options(game.channel) == []

    # Trying to draw an invalid option, or invalid channel should return None
    assert database.draw(987, options[0][0]) is None
    assert database.draw(game.channel, 987) is None


def test_draw_prematurely(mocker, database, game):
    """
    The GM can force a draw, it's important to check that the drawing function is doing the correct checks
    to avoid getting into an infinite loop
    """
    p1, p2, p3 = game.players
    options_names = ("Upper Body", "Lower Body")
    options = []
    for o in options_names:
        options.append((database.create_option(game.channel, o, game.players), o))

    assert database.get_unanswered_options(game.channel) == options
    assert database.get_waiting_players(game.channel) == set(game.players)

    p1_u_answer = "Player1's Upper answer"
    p1_l_answer = "Player1's Lower answer"
    assert database.add_answer(p1, p1_u_answer) == (game.channel, "Lower Body", 2)
    assert database.get_waiting_players(game.channel) == set(game.players)
    assert database.add_answer(p1, p1_l_answer) == (game.channel, None, 2)
    assert database.get_waiting_players(game.channel) == set([p2, p3])
    assert database.add_answer(p1, "wrong") == (None, None, None)

    # Only player 1 answered, we need at least two options to perform a draw
    assert database.draw(game.channel, options[0][0]) is False

    p2_u_answer = "Player2's Upper answer"
    assert database.add_answer(p2, p2_u_answer) == (game.channel, "Lower Body", 1)
    assert database.get_waiting_players(game.channel) == set([p2, p3])

    # Now that player 2 answered we can perform a draw even if player 3 hasn't, notice that is getting drawn by choices
    choice_mock = mocker.patch.object(random, 'choice', side_effect=[database.DOUBLE, p2, p3, database.DOUBLE, p3, p1])
    assert database.draw(game.channel, options[0][0]) == [(p1, p1_u_answer), (p2, p2_u_answer)]