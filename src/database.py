import json
import random
import sqlite3
from datetime import datetime

DOUBLE = 0

db = None
c = None


def init(database_uri):
    global db
    global c

    db = sqlite3.connect(database_uri)
    db.execute("PRAGMA foreign_keys = ON")
    c = db.cursor()

    # A game
    c.execute('''CREATE TABLE IF NOT EXISTS game(
    channel INTEGER PRIMARY KEY,
    gm TEXT NOT NULL
    )''')

    # A player of a game
    c.execute('''CREATE TABLE IF NOT EXISTS player(
    id INTEGER PRIMARY KEY,
    game INTEGER NOT NULL,
    player TEXT,
    FOREIGN KEY(game) REFERENCES game(channel) ON DELETE CASCADE
    )''')

    # The bowl with the names
    c.execute('''CREATE TABLE IF NOT EXISTS bowl(
    game INTEGER NOT NULL UNIQUE,
    bowl JSON NOT NULL DEFAULT [],
    used JSON NOT NULL DEFAULT [],
    FOREIGN KEY(game) REFERENCES game(channel) ON DELETE CASCADE
    )''')

    # An option given by the GM
    c.execute('''CREATE TABLE IF NOT EXISTS option(
    id INTEGER PRIMARY KEY,
    name TEXT,
    game INTEGER NOT NULL,
    answered DATE DEFAULT NULL,
    FOREIGN KEY(game) REFERENCES game(channel) ON DELETE CASCADE
    )''')

    #
    c.execute('''CREATE TABLE IF NOT EXISTS waiting(
    id INTEGER PRIMARY KEY,
    option INTEGER NOT NULL,
    player INTEGER NOT NULL,
    FOREIGN KEY(option) REFERENCES option(id) ON DELETE CASCADE
    )''')

    # An answer to an option given by a player
    c.execute('''CREATE TABLE IF NOT EXISTS answer(
    option INTEGER NOT NULL,
    player INTEGER NOT NULL,
    answer TEXT NOT NULL,
    FOREIGN KEY(option) REFERENCES option(id) ON DELETE CASCADE
    )''')

    # Save (commit) the changes
    db.commit()


def start_game(channel, gm, players):
    c.execute('SELECT * FROM game WHERE channel=?', (channel,))
    game = c.fetchone()

    # A game already existed for this channel, delete it (so it deletes everything related to it in cascade)
    if game is not None:
        c.execute("DELETE from game WHERE channel=?", (channel,))

    # Insert the game
    c.execute("INSERT INTO game(channel, gm) VALUES (?, ?)", (channel, gm))

    # Insert all players
    p = [(channel, p) for p in players]
    c.executemany('INSERT INTO player(game,player) VALUES (?,?)', p)

    # Create the bowl
    bowl = []
    for p in players:
        # 4 entries for each player
        for i in range(4):
            bowl.append(p)

    # as many doubles as there are players
    for i in range(len(players)):
        bowl.append(DOUBLE)

    c.execute('INSERT INTO bowl(game,bowl, used) VALUES (?,?, ?)', (channel, json.dumps(bowl), json.dumps([])))

    # Save (commit) the changes
    db.commit()


def get_gm(channel):
    """
    Given a channel return the discord id of the gm

    :param channel:
    :return:
    """
    c.execute('SELECT gm FROM game WHERE channel=?', (channel,))
    game = c.fetchone()
    if game is None:
        return None
    return int(game[0])


def get_players(channel):
    """
    Given a channel return the list of discord id of players for that game

    :param channel:
    :return:
    """
    c.execute('SELECT player FROM player WHERE game=?', (channel,))
    players = [int(p[0]) for p in c.fetchall()]
    return players


def create_option(channel, name, players):
    """
    Creates an option and the associated waiting

    :param channel:
    :param name:
    :param players:
    :return:
    """
    c.execute('INSERT INTO option(game, name) VALUES(?, ?)', (channel, name))
    option = c.lastrowid

    p = [(option, p) for p in players]
    c.executemany('INSERT INTO waiting(option,player) VALUES (?,?)', p)

    db.commit()
    return option


def get_waiting_players(channel):
    c.execute('SELECT player FROM waiting WHERE option IN (SELECT id FROM option WHERE answered IS NULL AND game=?)',
              (channel,))
    waiting = set(i[0] for i in c.fetchall())
    return waiting


def add_answer(player, answer):
    """
    Adds an answer to the first waiting

    :param player:
    :param answer:
    :return: tuple(int or None, int or None, int or None, int or None) a tuple with:
             - the game id (channel), or None if we were not waiting from this player
             - the next waiting name, or None if we're not waiting anything else from this player
             - the amount of other waitings, or None if we were not waiting from this player
    """
    c.execute('SELECT id, option FROM waiting WHERE player=?', (player,))
    waiting = c.fetchall()
    if not waiting:
        return None, None, None
    waiting_id, option = waiting.pop(0)
    c.execute('INSERT INTO answer(option, player, answer) VALUES(?, ?, ?)', (option, player, answer))
    c.execute('DELETE FROM waiting WHERE id=?', (waiting_id,))
    db.commit()

    c.execute('SELECT game FROM option WHERE id=?', (option,))
    game = c.fetchone()[0]

    next_option = None
    if len(waiting) > 0:
        _, next_option_id = waiting.pop(0)
        c.execute('SELECT name FROM option WHERE id=?', (next_option_id,))
        next_option = c.fetchone()[0]

    c.execute('SELECT id FROM waiting WHERE option=?', (option,))
    waiting = len(c.fetchall())

    return game, next_option, waiting


def get_unanswered_options(channel):
    c.execute('SELECT id, name FROM option WHERE game=? AND answered IS NULL', (channel,))
    return c.fetchall()


def get_option_names(channel, options):
    select_template = 'SELECT id, name FROM option WHERE game=? AND id IN ({})'
    parameter = ','.join(['?' for _ in options])
    expansions = [channel]
    expansions.extend(options)
    c.execute(select_template.format(parameter), expansions)
    return c.fetchall()


def draw(channel, option):
    """
    Draws from the bowl

    :return: list( tuple( int, str) ): A list of the answers given in tuple (user_id, text) form
    """
    # Prevent one channel redrawing from another by checking that the option matches the channel
    c.execute("SELECT id FROM option WHERE game=? AND id=?", (channel, option,))
    o = c.fetchone()
    if not o:
        return None
    # Since the GM can force the draw now first we need to check that there are enough responses
    c.execute("SELECT player FROM answer WHERE option=?", (option,))
    answers = c.fetchall()
    # We need at least 2 answers because there can be a double
    if len(answers) < 2:
        return False

    # Since this might be a premature draw build a list of invalid players to draw
    valid_players = [a[0] for a in answers]
    all_players = get_players(channel)
    invalid_players = [p for p in all_players if p not in valid_players]

    c.execute('SELECT bowl,used FROM bowl WHERE game=?', (channel,))
    bowl_row = c.fetchone()
    if bowl_row is None:
        return None

    bowl, used = [json.loads(b) for b in bowl_row]

    def reset_bowl_if_needed(invalid_choices=[]):
        nonlocal bowl
        nonlocal used
        diff_choices = set(bowl)
        available_choices = sum(i not in invalid_choices for i in diff_choices)
        if available_choices == 0:
            bowl.extend(used)
            used = []

    reset_bowl_if_needed(invalid_players)

    def draw_until_valid(invalid_choices):
        while True:
            choice = random.choice(bowl)
            if choice not in invalid_choices:
                return choice

    choice = draw_until_valid(invalid_players)
    bowl.remove(choice)
    used.append(choice)

    # Choice was a double, so we need to choose two non-double elements
    choices = []
    if choice == DOUBLE:
        while len(choices) < 2:
            # Avoid infinite loops by checking that there is a valid choice
            # The first pick of DOUBLE might have left only doubles or invalid players
            # also our first pick might have left the bowl with only the same player to draw from
            invalid_choices = invalid_players + [DOUBLE]
            invalid_choices.extend(choices)
            reset_bowl_if_needed(invalid_choices)

            # Push until we get something that is not a DOUBLE or an already drawn or invalid player
            choice = draw_until_valid(invalid_choices)

            bowl.remove(choice)
            used.append(choice)
            choices.append(choice)
    else:
        choices.append(choice)

    c.execute('UPDATE bowl SET bowl=?, used=? WHERE game=?', (json.dumps(bowl), json.dumps(used), channel,))
    # Mark the option as answered
    c.execute("UPDATE option SET answered=? where id=?", (datetime.now(), option,))
    db.commit()

    select_template = "SELECT player, answer FROM answer where option=? AND player IN ({})"
    parameter = ','.join(['?' for _ in choices])
    expansions = [option]
    expansions.extend(choices)
    c.execute(select_template.format(parameter), expansions)
    answers = c.fetchall()

    return answers
