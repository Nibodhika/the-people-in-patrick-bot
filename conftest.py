from collections import namedtuple

import pytest

Game = namedtuple('Game', 'channel gm players')


@pytest.fixture()
def database():
    """
    Creates an in-memory database for the tests and closes the connection afterwards
    :return:  database module
    """
    from src import database
    database.init(':memory:')
    # return the entire module which has the functions
    yield database
    # close the connection to the database, effectively deleting it
    database.db.close()


@pytest.fixture()
def game(database):
    """
    Creates a game with the following values:
        channel = 123
        gm = 25
        3 players:
            - 26
            - 32
            -105

        The values hold no meaning, they're just emulating discord ids

    :param database: fixture
    :return: Game tuple
    """
    game = Game(123, 25, [26, 32, 105])

    database.start_game(game.channel, game.gm, game.players)

    return game
